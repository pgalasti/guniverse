#include "GLFWWindow.h"

#include <thread>
#include <chrono>

using GApi::OpenGL::Window::GLFWGameWindow;
using Clock = std::chrono::high_resolution_clock;

GLFWGameWindow::GLFWGameWindow()
{
	m_LastDeltaTime = 0.0f;
	m_LastFPS = 0;
	m_pWindow = nullptr;
	m_bDebugMode = false;

	spPollInput = std::make_shared<PollInput>();
}

GLFWGameWindow::~GLFWGameWindow()
{

}

BOOL GLFWGameWindow::Initialize(const WindowOptions& options)
{
	if (!glfwInit())
		return FALSE;

	glfwWindowHint(GLFW_RESIZABLE, options.AllowResize ? GLFW_TRUE : GLFW_FALSE);

	m_pWindow = glfwCreateWindow((int)options.Width, (int)options.Height, options.WindowTitle, nullptr, nullptr);
	SetWindowTitle(options.WindowTitle);

	if (!m_pWindow)
	{
		this->Cleanup();
		return FALSE;
	}

	std::thread([this]() {
		glfwSetWindowSizeCallback(m_pWindow, GLFWGameWindow::WindowResizeCallback);
		glfwSetKeyCallback(m_pWindow, GLFWGameWindow::KeyCallback);
	}).detach();

	glfwMakeContextCurrent(m_pWindow);
	glfwSwapInterval(0);
	return TRUE;
}

BOOL GLFWGameWindow::Cleanup()
{
	if (m_pWindow)
		glfwDestroyWindow(m_pWindow);

	m_pGameEngine->Stop();

	glfwTerminate();
	return TRUE;
}


BOOL GLFWGameWindow::Start()
{
	m_bRunning = true;

	if (!m_pGameEngine->Start())
		return FALSE;

	Clock::time_point lastTime = Clock::now();
	do
	{
		auto deltaTimePoints = Clock::now() - lastTime;
		lastTime = Clock::now();

		const float32 deltaTime = (float32)std::chrono::duration_cast<std::chrono::microseconds>(deltaTimePoints).count()/1000;
		if (!Run(deltaTime))
			return FALSE;

	} while (m_bRunning);

	return TRUE;
}

BOOL GLFWGameWindow::HandleEvents()
{
	if (glfwWindowShouldClose(m_pWindow))
	{
		if (!this->OnClose())
			return FALSE;
	}

	double xPos, yPos;
	glfwGetCursorPos(m_pWindow, &xPos, &yPos);

	GlobalInputQueueEventMutex.lock();
	GlobalInputQueueEvent.clear();
	spPollInput->SetCursorPosition((float32)xPos, (float32)yPos);
	GlobalInputQueueEventMutex.unlock();

	if (!m_pGameEngine->HandleInputEvents(spPollInput.get()))
		return FALSE;

	return TRUE;
}

BOOL GLFWGameWindow::UpdateState()
{
	
	return TRUE;
}

BOOL GLFWGameWindow::Run(const float32 deltaTime)
{
	m_LastDeltaTime = deltaTime;

	if (!HandleEvents())
		return -1;

	if (!UpdateState())
		return -1;

	if (!m_pGameEngine->UpdateState(deltaTime))
		return -1;

	glfwSwapBuffers(m_pWindow);
	glfwPollEvents();
	
	m_LastFPS = (uint32)FPS();

	return TRUE;
}

void GLFWGameWindow::SetWindowTitle(const char8* pszName)
{
	glfwSetWindowTitle(m_pWindow, pszName);
	m_BaseWindowTitle = pszName;
}

BOOL GLFWGameWindow::GetWindowHandle(void* ptr)
{
	return TRUE;
}

void GLFWGameWindow::SetDebug(const bool enable)
{
	m_bDebugMode = enable;
}

float32 GLFWGameWindow::FPS() const
{
	static uint32 Frames = 0;
	static float32 deltaTime = 0.0f;

	++Frames;
	uint32 lastFPS = m_LastFPS;
	deltaTime += m_LastDeltaTime;

	if (deltaTime > 1000.0f)
	{
		lastFPS = Frames;

		Frames = 0;
		deltaTime = 0.0f;
		
		if (m_bDebugMode) 
		{
			const std::string debugTitle = m_BaseWindowTitle + std::string(" - FPS: ") + std::to_string(lastFPS);
			glfwSetWindowTitle(m_pWindow, debugTitle.c_str());
		}
	}
	
	return (float32)lastFPS;
}

bool GLFWGameWindow::IsRunning() const
{
	return true;
}

void GLFWGameWindow::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	static uint32 numPresses;
	InputEvent event;
	//event.number = ++numPresses;

	std::lock_guard<std::mutex> lock(GlobalInputQueueEventMutex);
	GlobalInputQueueEvent.push_back(event);
}

void GLFWGameWindow::WindowResizeCallback(GLFWwindow* pWindow, int width, int height)
{
}

BOOL GLFWGameWindow::OnClose()
{
	// Shutdown subsystems
	m_bRunning = false;
	return TRUE;
}

BOOL GLFWGameWindow::OnResize()
{

	return TRUE;
}

BOOL GLFWGameWindow::SetGameEngine(IGameEngine* pEngine)
{
	m_pGameEngine = pEngine;
	return TRUE;
}