#include "OpenGLModel.h"

using GApi::Graphics::Model;
using GApi::Graphics::Vertex3D;
using GApi::Graphics::OpenGL::OpenGLModel;

OpenGLModel::OpenGLModel(const Model<float32>& model)
	: m_VertexArrayObject(0), m_VertexArrayBuffer(0), m_ElementArrayBuffer(0), m_ColorArrayBuffer(0), m_PositionIndex(0), m_ShaderProgram(0), m_ColorIndex(-1)
{
	m_Model = model;

	const uint32 nVerticies = (uint32)m_Model.Positions.size();
	const uint32 nIndices = (uint32)m_Model.Indices.size();
	const uint32 nColors = (uint32)m_Model.Colors.size()*3;

	glGenVertexArrays(1, &m_VertexArrayObject);
	glBindVertexArray(m_VertexArrayObject);
	
	glGenBuffers(1, &m_VertexArrayBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexArrayBuffer);
	glBufferData(GL_ARRAY_BUFFER, nVerticies * sizeof(Vertex3D<float32>), &m_Model.Positions[0], GL_STATIC_DRAW);

 	glGenBuffers(1, &m_ElementArrayBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ElementArrayBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, nIndices * sizeof(ushort16), &m_Model.Indices[0], GL_STATIC_DRAW);

	if (nColors)
	{
		glGenBuffers(1, &m_ColorArrayBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_ColorArrayBuffer);
		glBufferData(GL_ARRAY_BUFFER, nColors * sizeof(RGBColor), &m_Model.Colors[0], GL_STATIC_DRAW);
	}

	glBindVertexArray(0);
}
OpenGLModel::~OpenGLModel()
{
	Cleanup();
}

BOOL OpenGLModel::DrawWith(const GLuint shaderProgram)
{
	const uint32 nVerticies = (uint32)m_Model.Positions.size();

	glBindVertexArray(m_VertexArrayObject);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexArrayBuffer);

	m_ShaderProgram = shaderProgram;
	
	m_PositionIndex = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(m_PositionIndex);
	glVertexAttribPointer(m_PositionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	m_ColorIndex = glGetAttribLocation(shaderProgram, "vertexColor");
	if (m_ColorIndex > -1)
	{
		glEnableVertexAttribArray(m_ColorIndex);
		glBindBuffer(GL_ARRAY_BUFFER, m_ColorArrayBuffer);
		glVertexAttribPointer(m_ColorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}

	return TRUE;
}

BOOL OpenGLModel::DisableDraw()
{
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexArrayBuffer);

	if(m_PositionIndex)
		glDisableVertexAttribArray(m_PositionIndex);

	return TRUE;
}

BOOL OpenGLModel::Cleanup()
{

	m_Model.Positions.clear();
	m_Model.Indices.clear();

	if(m_ColorArrayBuffer)
		glDeleteVertexArrays(1, &m_ColorArrayBuffer);
	m_ColorArrayBuffer = 0;

	if(m_ElementArrayBuffer)
		glDeleteVertexArrays(1, &m_ElementArrayBuffer);
	m_ElementArrayBuffer = 0;

	if (m_VertexArrayBuffer)
		glDeleteBuffers(1, &m_VertexArrayBuffer);
	m_VertexArrayBuffer = 0;

	if(m_VertexArrayObject)
		glDeleteVertexArrays(1, &m_VertexArrayObject);
	m_VertexArrayObject = 0;

	m_ColorIndex = -1;

	return TRUE;
}