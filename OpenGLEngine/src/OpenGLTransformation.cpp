#include "OpenGLTransformation.h"

#include <glm/gtc/matrix_transform.hpp>

using GApi::Graphics::Transformation;
using GApi::Graphics::OpenGL::OpenGLTransformation;

OpenGLTransformation::OpenGLTransformation()
{
	this->ModelMatrix = glm::mat4x4(1.0f);
}

OpenGLTransformation::OpenGLTransformation(const Transformation& transformation)
{
	ApplyTransformation(transformation);
}

OpenGLTransformation::~OpenGLTransformation()
{

}

void OpenGLTransformation::ApplyTransformation(const Transformation& transformation)
{
	this->ModelMatrix = glm::mat4x4(1.0f);
	glm::mat4x4 scaleMatrix(1.0f);
	glm::mat4x4 translateMatrix(1.0f);
	glm::mat4x4 rotateMatrix(1.0f);

	if (transformation.HasScale)
	{
		scaleMatrix = glm::scale(scaleMatrix, glm::vec3(transformation.Scale[0], transformation.Scale[1], transformation.Scale[2]));
	}

	if (transformation.HasTranslate)
	{
		translateMatrix = glm::translate(translateMatrix, glm::vec3(transformation.Translate[0], transformation.Translate[1], transformation.Translate[2]));
	}

	if (transformation.HasRotate)
	{
		if (transformation.HasRotateX)
			rotateMatrix *= glm::rotate(rotateMatrix, transformation.Rotate[0], X_AXIS);
		if (transformation.HasRotateY)
			rotateMatrix *= glm::rotate(rotateMatrix, transformation.Rotate[1], Y_AXIS);
		if (transformation.HasRotateZ)
			rotateMatrix *= glm::rotate(rotateMatrix, transformation.Rotate[2], Z_AXIS);
	}
	
	this->ModelMatrix = translateMatrix*rotateMatrix*scaleMatrix;
}

const glm::mat4 OpenGLTransformation::GetGlmMatrix() const
{
	return this->ModelMatrix;
}

BOOL OpenGLTransformation::Cleanup()
{

	return TRUE;
}