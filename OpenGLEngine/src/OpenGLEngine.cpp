#include "OpenGLEngine.h"
#include "OpenGLModel.h"
#include "GFile.h"

#include <glm/gtc/matrix_transform.hpp>

using namespace GApi::Graphics::OpenGL;

BOOL OpenGLEngine::OnResize(const float32 width, const float32 height)
{
	this->m_Options.width = width;
	this->m_Options.height = height;

	return TRUE;
}

BOOL OpenGLEngine::SetOptions(const GraphicsEngineOptions& options)
{
	m_Options = options;
	return TRUE;
}

BOOL OpenGLEngine::SetWindowHandle(void* ptr) 
{
	return TRUE;
}

BOOL OpenGLEngine::Startup()
{
	m_pShaderIDGenerator = new IDGenerator<ResourceID>(100);

	gl3wInit();

	m_Camera.Initialize();

	return TRUE;
}

BOOL OpenGLEngine::Shutdown()
{
	auto DeleteShaders = [](ResourceToShaderMap* map) {
		std::vector<GLuint> toDelete;
		for (auto iter = map->begin(); iter != map->end(); ++iter) 
			toDelete.push_back(iter->second);

		for (GLuint shader : toDelete)
			glDeleteShader(shader);

		map->clear();
	};
	DeleteShaders(&m_VertexShaderMap);
	DeleteShaders(&m_TesselationShaderMap);
	DeleteShaders(&m_GeometryShaderMap);
	DeleteShaders(&m_FragmentShaderMap);

	for (auto iter : m_ShaderProgramMap)
		glDeleteProgram(iter.second);
	m_ShaderProgramMap.clear();

	m_ModelResourceSubsystem.ClearResources();
	m_TransformationResourceSubsystem.ClearResources();

	SAFE_DELETE(m_pShaderIDGenerator);
	return TRUE;
}

BOOL OpenGLEngine::RegisterModel(const Model<float32>& model, ResourceID& resourceID)
{
	const ResourceID newResource = m_ModelResourceSubsystem.AddResource(std::shared_ptr<OpenGLModel> (new OpenGLModel(model)));
	resourceID = newResource;

	return TRUE;
}

BOOL OpenGLEngine::DeregisterModel(const ResourceID resourceID)
{
	m_ModelResourceSubsystem.EraseResource(resourceID);
	return TRUE;
}

BOOL OpenGLEngine::RegisterShader(const ShaderStage shader, const std::string& path, ResourceID& resourceID)
{
	char8 shaderSource[8192];
	if (!GApi::IO::LoadFile(path.c_str(), shaderSource, 8192))
		return FALSE;
	const GLchar* pFSSource = (const GLchar*)shaderSource;
	
	ResourceToShaderMap* pShaderMap = nullptr;
	GLenum glShaderStage;
	switch (shader)
	{
	case Vertex:
		glShaderStage = GL_VERTEX_SHADER;
		pShaderMap = &m_VertexShaderMap;
		break;
	case Tesselation:
		glShaderStage = GL_TESS_CONTROL_SHADER;
		pShaderMap = &m_TesselationShaderMap;
		break;
	case Geometry:
		glShaderStage = GL_GEOMETRY_SHADER;
		pShaderMap = &m_GeometryShaderMap;
		break;
	case Pixel:
		glShaderStage = GL_FRAGMENT_SHADER;
		pShaderMap = &m_FragmentShaderMap;
		break;
	}

	const GLuint shaderID = glCreateShader(glShaderStage);
	glShaderSource(shaderID, 1, &pFSSource, NULL);
	glCompileShader(shaderID);

	GLint bCompileSuccess = 0;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &bCompileSuccess);
	if (!GL_BOOLEAN_TRUE(bCompileSuccess))
	{
		return FALSE;
	}

	resourceID = m_pShaderIDGenerator->GetNextId();
	pShaderMap->insert(std::pair<ResourceID, GLuint>(resourceID, shaderID));
	return TRUE;
}

BOOL OpenGLEngine::DeregisterShader(const ResourceID resourceID)
{
	GLuint shaderID;
	ResourceToShaderMap* pMap;
	if (!FindShaderID(resourceID, shaderID, &pMap))
		return FALSE;

	pMap->erase(resourceID);
	m_pShaderIDGenerator->Release(resourceID);
	glDeleteShader(shaderID);

	return TRUE;
}

BOOL OpenGLEngine::LinkShaders(const std::string& alias, std::vector<ResourceID>& resources)
{
	GLuint program = glCreateProgram();

	GLuint shaderID;
	ResourceToShaderMap* pMap;
	for (const ResourceID resource : resources)
	{
		if (!FindShaderID(resource, shaderID, &pMap))
			return FALSE;
		glAttachShader(program, shaderID);
	}

	glLinkProgram(program);

	m_ShaderProgramMap[alias] = program;

	return TRUE;
}

BOOL OpenGLEngine::DeleteLinkedShaders(const std::string& alias)
{
	auto iter = m_ShaderProgramMap.find(alias);
	if (iter == m_ShaderProgramMap.end())
		return FALSE;

	const GLuint shaderProgram = iter->second;

	OpenGLModelPtr modelPtr;
	const auto resourcesInUse = m_ModelResourceSubsystem.GetResourcesInUse(); 
	for (const ResourceID resource : resourcesInUse)
	{
		if (!m_ModelResourceSubsystem.GetResource(resource, modelPtr))
			return FALSE;

		if (modelPtr->GetShaderProgram() == shaderProgram)
		{
			if (!modelPtr->DisableDraw())
				return FALSE;
		}
	}

	glDeleteProgram(iter->second);
	m_ShaderProgramMap.erase(alias);

	return TRUE;
}

BOOL OpenGLEngine::LinkShaderToModel(const std::string& alias, const ResourceID resourceID)
{
	const auto iter = m_ShaderProgramMap.find(alias);
	if (iter == m_ShaderProgramMap.end())
		return FALSE;

	GLuint program;
	m_ModelToShaderProgramMap[resourceID] = program = iter->second;

	OpenGLModelPtr modelPtr = nullptr;
	if (!m_ModelResourceSubsystem.GetResource(resourceID, modelPtr))
		return FALSE;

	if (!modelPtr->DrawWith(program))
		return FALSE;

	return TRUE;
}

BOOL OpenGLEngine::ApplyTransformation(const Transformation& transformation, const ResourceID modelResourceID)
{
	OpenGLTransformationPtr transformationPtr = std::make_shared<OpenGLTransformation>();
	transformationPtr->ApplyTransformation(transformation);

	const ResourceID transformationResourceID = m_TransformationResourceSubsystem.AddResource(transformationPtr);
	m_ModelToTransformationMultiMap.insert(std::pair<ResourceID, ResourceID>(modelResourceID, transformationResourceID));

	return TRUE;
}

BOOL OpenGLEngine::Render()
{
	ZeroMemory(&m_EngineState, sizeof(EngineStateInfo));

	static const RGBAColor green = RGBAColor::DefaultBlue();
	if (!ClearScreen(green))
		return FALSE;

	OpenGLModelPtr modelPtr = nullptr;
	const auto resourcesInUse = m_ModelResourceSubsystem.GetResourcesInUse();
	
	for (const ResourceID resource : resourcesInUse)
	{
		if (!m_ModelResourceSubsystem.GetResource(resource, modelPtr))
			return FALSE;

		glBindVertexArray(modelPtr->GetVertexArrayObject());

		GLuint program = m_ModelToShaderProgramMap[resource];
		glUseProgram(program);
		GLuint matrixId = glGetUniformLocation(program, "MVP");

		std::vector<OpenGLTransformationPtr> transformations = GetTransformation(resource);
		for(OpenGLTransformationPtr spTransformation : transformations)
		{
			const glm::mat4 model = spTransformation->GetGlmMatrix();
			const glm::mat4 view = ViewMatrix(m_Camera);
			const glm::mat4 projection = ProjectionMatrix(m_Camera);
			const glm::mat4 mvp = projection * view * model;

			glUniformMatrix4fv(matrixId, 1, GL_FALSE, &mvp[0][0]);
			glDrawElements(GL_TRIANGLES, modelPtr->GetNumberIndices(), GL_UNSIGNED_SHORT, (GLvoid*)0);

			m_EngineState.Verticies += modelPtr->GetNumberVerticies();
			m_EngineState.Indices += modelPtr->GetNumberIndices();
		}
	}

	m_EngineState.Transformations += (uint32)m_ModelToTransformationMultiMap.size();

	m_ModelToTransformationMultiMap.clear();
	m_TransformationResourceSubsystem.ClearResources();

	return TRUE;
}

BOOL OpenGLEngine::ClearScreen(const RGBAColor& clearColor)
{
	GLfloat glColor[4] = {clearColor.r,clearColor.g, clearColor.b, clearColor.a};
	glClearBufferfv(GL_COLOR, 0, glColor);
	return TRUE;
}

BOOL OpenGLEngine::SetRendering(const RenderOptions& options)
{
	switch (options.PolygonRendering)
	{
	case PolygonRenderEnum::Point:
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		break;
	case PolygonRenderEnum::Line:
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	case PolygonRenderEnum::Fill:
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	}
	
	return TRUE;
}

std::string OpenGLEngine::GetEngineDescription() const
{
	return std::string("OpenGL Test Engine v0.1");
}

EngineStateInfo OpenGLEngine::GetEngineState() const
{
	return m_EngineState;
}

std::vector<OpenGLTransformationPtr> OpenGLEngine::GetTransformation(const ResourceID modelResourceID)
{
	std::vector<OpenGLTransformationPtr> transformations;

	auto lowerBound = m_ModelToTransformationMultiMap.lower_bound(modelResourceID);
	auto upperBound = m_ModelToTransformationMultiMap.upper_bound(modelResourceID);

	ResourceID transformationId;
	for (auto iter = lowerBound; iter != upperBound; ++iter)
	{
		transformationId = iter->second;
		OpenGLTransformationPtr spTransformation;
		m_TransformationResourceSubsystem.GetResource(transformationId, spTransformation);
		transformations.push_back(spTransformation);
	}

	return transformations;
}

BOOL OpenGLEngine::FindShaderID(const ResourceID shaderResourceID, GLuint& shaderID, ResourceToShaderMap** pMap)
{
	auto FindShaderResource = [](ResourceToShaderMap* pMap, const ResourceID shaderResourceID) -> GLuint {
		auto iter = pMap->find(shaderResourceID);
		if (iter == pMap->end())
			return 0;
		return iter->second;
	};
	
	std::vector<ResourceToShaderMap*> maps = { &m_VertexShaderMap, &m_FragmentShaderMap, &m_TesselationShaderMap, &m_GeometryShaderMap };
	for (ubyte8 i = 0; i < maps.size(); ++i)
	{
		*pMap = maps[i];
		shaderID = FindShaderResource(*pMap, shaderResourceID);
		if (shaderID)
			return TRUE;
	}

	return FALSE;
}

glm::mat4 OpenGLEngine::ViewMatrix(const Camera& camera)
{
	const Vector3D postion = camera.GetPosition();
	const Vector3D look = camera.GetLook();
	const Vector3D up = camera.GetUp();

	return glm::lookAt(
		glm::vec3(postion.data[0], postion.data[1], postion.data[2]),	// Position
		glm::vec3(look.data[0], look.data[1], look.data[2]),			// Look
		glm::vec3(up.data[0], up.data[1], up.data[2])					// Up
	);
}

glm::mat4 OpenGLEngine::ProjectionMatrix(const Camera& camera)
{
	return glm::perspective(camera.GetFieldOfViewRadians(), camera.GetAspectRatio(), camera.GetNearPlane(), camera.GetFarPlane());
}