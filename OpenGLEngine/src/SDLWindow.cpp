#include "SDLWindow.h"

using GApi::OpenGL::Window::SDLGameWindow;

SDLGameWindow::SDLGameWindow()
{
}

SDLGameWindow::~SDLGameWindow()
{

}

BOOL SDLGameWindow::Initialize(const WindowOptions& options)
{
	return TRUE;
}

BOOL SDLGameWindow::Cleanup()
{
	return TRUE;
}

BOOL SDLGameWindow::Start()
{
	return TRUE;
}

BOOL SDLGameWindow::HandleEvents()
{

	return TRUE;
}

BOOL SDLGameWindow::UpdateState()
{

	return TRUE;
}

BOOL SDLGameWindow::Run(const float32 deltaTime)
{
	return TRUE;
}

void SDLGameWindow::SetWindowTitle(const char8* pszName)
{

}

//BOOL SDLGameWindow::SetGameEngine(IGameEngine* pEngine)
//{
//	m_pGameEngine = pEngine;
//	return TRUE;
//};

float32 SDLGameWindow::FPS()
{
	return 1.0f;
}

bool SDLGameWindow::IsRunning() const
{
	return false;
}

BOOL SDLGameWindow::GetWindowHandle(void* ptr)
{
	return TRUE;
}

SDL_Window* SDLGameWindow::castSDLWindowPtr() const
{
	return nullptr;
}