#ifndef G_OPEN_GL_TRANSFORMATION_H
#define G_OPEN_GL_TRANSFORMATION_H

#include "Defines.h"

#include <glm/glm.hpp>

#include "GTransformation.h"
#include "Resource.h"

namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			using GApi::Graphics::Transformation;

			const static glm::vec3 X_AXIS(1.0f, 0.0f, 0.0f);
			const static glm::vec3 Y_AXIS(0.0f, 1.0f, 0.0f);
			const static glm::vec3 Z_AXIS(0.0f, 0.0f, 1.0f);
			
			class LibExport OpenGLTransformation : implements Resource
			{
			public:

				DISABLE_COPY_CONSTRUCTOR(OpenGLTransformation);

				OpenGLTransformation();
				OpenGLTransformation(const Transformation& transformation);
				~OpenGLTransformation();

				inline const glm::mat4 GetGlmMatrix() const;
				void ApplyTransformation(const Transformation& transformation);

				virtual BOOL Cleanup() override;

			private:
				glm::mat4 ModelMatrix;
			};


		}
	}
}


#endif //G_OPEN_GL_TRANSFORMATION_H