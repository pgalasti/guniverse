#ifndef G_RESOURCE_SUBSYSTEM_H
#define G_RESOURCE_SUBSYSTEM_H

#include "Defines.h"

#include <unordered_map>
#include <map>
#include <memory>
#include <future>
#include <vector>

#include "IDGenerator.h"
#include "Resource.h"
#include "GGraphicsEngine.h"

namespace GApi {
	namespace Graphics {
		namespace OpenGL {
			
			using GApi::Util::IDGenerator;

			template <typename ResourceInterface, typename std::enable_if<std::is_base_of<Resource, ResourceInterface>::value>::type* = nullptr>
			class LibExport ResourceSubsystem
			{
				typedef std::shared_ptr<ResourceInterface> ResourceInterfacePtr;
				typedef std::unordered_map<ResourceID, ResourceInterfacePtr> ResourceMap;
				
			private:
				IDGenerator<ResourceID>* m_pIDGenerator;
				ResourceMap m_LoadedResources;

			public:
				ResourceSubsystem(const ResourceID startValue = 1)
				{
					m_pIDGenerator = new IDGenerator<ResourceID>(startValue);
				}

				~ResourceSubsystem() 
				{
					SAFE_DELETE(m_pIDGenerator)
				}

				ResourceID AddResource(ResourceInterfacePtr pResource)
				{
					const ResourceID id = m_pIDGenerator->GetNextId();

					m_LoadedResources[id] = pResource;

					return id;
				}

				BOOL GetResource(const ResourceID resource, ResourceInterfacePtr& pResource)
				{
					//auto resourceIter = m_LoadedResources.find(resource);
					//if (resourceIter == m_LoadedResources.end())
					//{
					//	if (m_LoadingResources.find(resource) == m_LoadingResources.end())
					//	{
					//		return FALSE;
					//	}

					//	if (!LoadPendingResource(resource))
					//		return FALSE;

					//	resourceIter = m_LoadedResources.find(resource);
					//}

					auto resourceIter = m_LoadedResources.find(resource);
					if (resourceIter == m_LoadedResources.end())
						return FALSE;

					pResource = resourceIter->second;

					return TRUE;
				}

				const std::set<ResourceID> GetResourcesInUse()
				{
					return m_pIDGenerator->GetInUseIDs();
				}

				void EraseResource(const ResourceID resource)
				{
					ResourceInterfacePtr pResource;
					GetResource(resource, pResource);
					pResource->Cleanup();
					m_LoadedResources.erase(resource);
				}

				void ClearResources()
				{
					std::vector<ResourceID> resourcesToDelete;
					for (auto iter : m_LoadedResources)
						resourcesToDelete.push_back(iter.first);

					for (const ResourceID resource : resourcesToDelete)
						EraseResource(resource);

					m_LoadedResources.clear();
					m_pIDGenerator->Reset(1);
				}

				//BOOL LoadPendingResource(const ResourceID resource)
				//{
				//	auto iter = m_LoadingResources.find(resource);
				//	ResourceInterfacePtr pResource(iter->second.get());

				//	m_LoadedResources[resource] = pResource;

				//	m_LoadingResources.erase(resource);

				//	return TRUE;
				//}

			};

		}
	}
}
#endif // G_RESOURCE_SUBSYSTEM_H 
