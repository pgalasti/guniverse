#ifndef G_GLFW_WINDOW_H
#define G_GLFW_WINDOW_H
#include "Defines.h"

#ifdef _WIN32
#pragma warning(disable: 4099)
#endif //_WIN32

#include <GLFW/glfw3.h>
#include <memory>
#include <deque>
#include <mutex>

#include "GWindow.h"
#include "GGameWindow.h"
#include "IGameEngine.h"

using GApi::UI::WindowOptions;
using GApi::UI::IGameWindow;
using GApi::Gaming::InputEvent;
using GApi::Gaming::IGameEngine;
using GApi::System::PollInput;

namespace GApi {
	namespace OpenGL {
		namespace Window {

			// Input Events
			static std::deque<InputEvent> GlobalInputQueueEvent;
			static std::mutex GlobalInputQueueEventMutex;

			class LibExport GLFWGameWindow : public IGameWindow
			{
			public:
				GLFWGameWindow();
				virtual ~GLFWGameWindow();

				// Startup/Close
				virtual BOOL Initialize(const WindowOptions& options) override;
				virtual BOOL Start() override;
				virtual BOOL Cleanup() override;
				
				// On window event
				virtual BOOL OnClose() override;
				virtual BOOL OnResize() override;

				// Misc
				virtual inline void SetWindowTitle(const char8* pszName) override;
				virtual BOOL GetWindowHandle(void* ptr) override;
				virtual void SetDebug(const bool enable) override;

				// Game state
				virtual inline bool IsRunning() const override;
				virtual float32 FPS() const override;
				virtual BOOL HandleEvents() override;
				virtual BOOL UpdateState() override;
				virtual BOOL Run(const float32 deltaTime) override;

				virtual BOOL SetGameEngine(IGameEngine* pEngine) override;

			private:
				
				GLFWwindow* m_pWindow;
				IGameEngine* m_pGameEngine;

				bool m_bRunning;
				bool m_bDebugMode;

				float32 m_LastDeltaTime;
				uint32 m_LastFPS;

				std::string m_BaseWindowTitle;

				static void KeyCallback(GLFWwindow* pWindow, int key, int scancode, int action, int mods);
				static void WindowResizeCallback(GLFWwindow* pWindow, int width, int height);

				std::shared_ptr<PollInput> spPollInput;
			};

		}
	}
}

#endif //G_GLFW_WINDOW_H
