#ifndef G_SDL_WINDOW_H
#define G_SDL_WINDOW_H

#include <SDL.h>
#include "Defines.h"
#include "GWindow.h"
#include "GGameWindow.h"

using GApi::UI::WindowOptions;
using GApi::UI::IGameWindow;

namespace GApi {
	namespace OpenGL {
		namespace Window {

			class LibExport SDLGameWindow : public IGameWindow
			{
			public:
				SDLGameWindow();
				virtual ~SDLGameWindow();

				virtual BOOL Initialize(const WindowOptions& options) override;
				virtual BOOL Cleanup() override;
				virtual BOOL Start() override;
				virtual BOOL HandleEvents() override;
				virtual BOOL UpdateState() override;
				virtual BOOL Run(const float32 deltaTime) override;
				virtual inline void SetWindowTitle(const char8* pszName) override;
				//virtual inline BOOL SetGameEngine(IGameEngine* pEngine) override;
				virtual BOOL GetWindowHandle(void* ptr) override;
				virtual float32 FPS();
				virtual inline bool IsRunning() const;

			private:
				SDL_Window * castSDLWindowPtr() const;
				//SDL_GLContext m_glContext;

				SDL_Renderer* m_pRenderer;


			};
		}
	}
}

#endif //G_SDL_WINDOW_H