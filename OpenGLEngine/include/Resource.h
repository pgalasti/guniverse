#ifndef G_Resource_H
#define G_Resource_H

#include "Defines.h"

#include <memory>
#include <vector>

namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			class LibExport Resource
			{
			public:
				virtual BOOL Cleanup() = VIRTUAL;
			};

		}
	}
}


#endif
