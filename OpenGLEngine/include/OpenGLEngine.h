#ifndef G_OPENGL_ENGINE_H
#define G_OPENGL_ENGINE_H
#include "Defines.h"

#include <unordered_set>

#include "GGraphicsEngine.h"
#include "ResourceSubsystem.h"
#include "OpenGLModel.h"
#include "GTransformation.h"
#include "OpenGLTransformation.h"
#include "GCamera.h"

#include <GL/gl3w.h>

#define GL_BOOLEAN_TRUE(glint) glint == GL_TRUE
#define GL_BOOLEAN_FALSE(glint) glint == GL_FALSE
#define GL_HAS_ERROR(glint) glint == -1


namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			using namespace GApi::Graphics;

			typedef std::shared_ptr<OpenGLModel> OpenGLModelPtr;
			typedef std::shared_ptr<OpenGLTransformation> OpenGLTransformationPtr;

			typedef std::unordered_map<ResourceID, GLuint> ResourceToShaderMap;
			typedef std::unordered_map<std::string, GLuint> ShaderProgramMap;
			typedef std::unordered_map<ResourceID, GLuint> ModelToShaderMap;
			typedef std::unordered_map<ResourceID, ResourceID> ResourceToResourceMap;
			typedef std::unordered_multimap<ResourceID, ResourceID> ResourceToResourceMultiMap;

			class LibExport OpenGLEngine : implements IGraphicsEngine
			{
				// Window Management
				virtual BOOL OnResize(const float32 width, const float32 height) override;

				// Options
				virtual BOOL SetOptions(const GraphicsEngineOptions& options) override;
				virtual BOOL SetWindowHandle(void* ptr) override;

				// Initialize/Shutdown
				virtual BOOL Startup() override;
				virtual BOOL Shutdown() override;

				// Resource Registration
				virtual BOOL RegisterModel(const Model<float32>& model, ResourceID& resourceID) override;
				virtual BOOL DeregisterModel(const ResourceID resourceID) override;

				virtual BOOL RegisterShader(const ShaderStage shader, const std::string& path, ResourceID& resourceID) override;
				virtual BOOL DeregisterShader(const ResourceID resourceID) override;

				virtual BOOL LinkShaders(const std::string& alias, std::vector<ResourceID>& resources) override;
				virtual BOOL DeleteLinkedShaders(const std::string& alias) override;
				virtual BOOL LinkShaderToModel(const std::string& alias, const ResourceID resourceID) override;

				// State Modification
				virtual BOOL ApplyTransformation(const Transformation& transformation, const ResourceID modelResourceID) override;

				// Rendering
				virtual BOOL Render() override;
				virtual BOOL ClearScreen(const RGBAColor& clearColor) override;
				virtual BOOL SetRendering(const RenderOptions& options) override;

				// Misc
				virtual std::string GetEngineDescription() const override;
				virtual EngineStateInfo GetEngineState() const override;
				
				Camera* GetCameraPtr() override { return &m_Camera; }

			private:
				// Resource Subsystems
				ResourceSubsystem<OpenGLModel> m_ModelResourceSubsystem;
				ResourceSubsystem<OpenGLTransformation> m_TransformationResourceSubsystem;

				// Resource Management Mappings
				IDGenerator<ResourceID>* m_pShaderIDGenerator;								// Shader ID
				ShaderProgramMap m_ShaderProgramMap;										// std::string alias to GLuint program reference
				ModelToShaderMap m_ModelToShaderProgramMap;									// Model ResourceID to GLuint program reference

				ResourceToShaderMap m_VertexShaderMap;										// Shader ResourceID to GLuint shader reference
				ResourceToShaderMap m_TesselationShaderMap;									// Shader ResourceID to GLuint shader reference
				ResourceToShaderMap m_GeometryShaderMap;									// Shader ResourceID to GLuint shader reference
				ResourceToShaderMap m_FragmentShaderMap;									// Shader ResourceID to GLuint shader reference

				ResourceToResourceMap m_ModelToTransformationMap;							// Model ResourceID to Transformation ResourceID
				ResourceToResourceMultiMap m_ModelToTransformationMultiMap;					// Models ResourceID to many Transformation ResourceID

				// Helper Functions
				std::vector<OpenGLTransformationPtr> GetTransformation(const ResourceID modelResourceID);
				BOOL FindShaderID(const ResourceID shaderResourceID, GLuint& shaderID, ResourceToShaderMap** pMap);
				glm::mat4 ViewMatrix(const Camera& camera);
				glm::mat4 ProjectionMatrix(const Camera& camera);

				GraphicsEngineOptions m_Options;
				Camera m_Camera;
				EngineStateInfo m_EngineState;
			};

		}
	}
}





#endif //G_OPENGL_ENGINE_H
