#ifndef G_OPEN_GL_MODEL_H
#define G_OPEN_GL_MODEL_H

#include "Defines.h"

#include <GL/gl3w.h>
#include "GL/gl.h"

#include "Resource.h"
#include "GModel.h"


namespace GApi {
	namespace Graphics {
		namespace OpenGL {

			using GApi::Graphics::Model;

			class LibExport OpenGLModel : implements Resource
			{
			public:
				//DISABLE_IMPLICIT_CONSTRUCTOR(OpenGLModel);
				//DISABLE_COPY_CONSTRUCTOR(OpenGLModel);

				OpenGLModel(const Model<float32>& model);
				~OpenGLModel();
				
				virtual BOOL Cleanup() override;

				BOOL DrawWith(const GLuint shaderProgram);
				BOOL DisableDraw();

				inline GLuint GetVertexArrayObject() const { return m_VertexArrayObject; }
				inline GLuint GetVertexArrayBuffer() const { return m_VertexArrayBuffer; }
				inline GLuint GetElementArrayBuffer() const { return m_ElementArrayBuffer; }
				inline GLuint GetColortArrayBuffer() const { return m_ColorArrayBuffer; }
				inline GLuint GetShaderProgram() const { return m_ShaderProgram; }

				inline uint32 GetNumberVerticies() const { return (uint32)m_Model.Positions.size(); }
				inline uint32 GetNumberIndices() const { return (uint32)m_Model.Indices.size(); }
				inline std::vector<ushort16> GetIndices() const { return m_Model.Indices; }

			private:
				Model<float32> m_Model;

				GLuint m_VertexArrayObject;
				GLuint m_VertexArrayBuffer;
				GLuint m_ElementArrayBuffer;
				GLuint m_ColorArrayBuffer;

				GLuint m_ShaderProgram;
				GLuint m_PositionIndex;
				GLint  m_ColorIndex;
			};

		}
	}
}

#endif //G_OPEN_GL_MODEL_H
