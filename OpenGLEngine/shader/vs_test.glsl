#version 420 core

attribute vec3 position;
attribute vec3 vertexColor;

uniform mat4 MVP;

out vec3 fragmentColor;

void main(void)
{
	gl_Position = MVP * vec4(position, 1.0);

	fragmentColor = vertexColor;
}