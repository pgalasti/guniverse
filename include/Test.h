#ifndef TEST_H
#define TEST_H

#include "Defines.h"
#include <iostream>
#include <glm.hpp>

class LibExport Test
{
public:
	Test();
	void DoTest();
};

#endif