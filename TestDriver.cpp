#include "Defines.h"
//#include <GL/gl3w.h>

#include <iostream>
#include <memory>

#include "OpenGLEngine.h"
#include "GWindow.h"
#include "GGameWindow.h"
#include "GLFWWindow.h"
#include "IGameEngine.h"
#include "GGraphicsEngine.h"
#include "GModel.h"
#include "ResourceSubsystem.h"
#include "GShapes.h"
#include "GCamera.h"
#include "GColor.h"
#include "FormatLoaders.h"

#include "Mark1Engine.h"

using GApi::Gaming::IGameEngine;
using GApi::Gaming::InputEvent;
using GApi::Graphics::IGraphicsEngine;
using GApi::Graphics::OpenGL::OpenGLEngine;

int main(int argc, char** argv) 
{

	//GApi::Graphics::ObjFormatLoader formatLoader;
	//GApi::Graphics::Model<float32> test;
	//if (!formatLoader.LoadFile("C:\\Users\\Paul\\Documents\\obj\\testing.obj", test))
	//	return 0;

	GApi::UI::WindowOptions windowOptions;
	strcpy(windowOptions.WindowTitle, "Paul is testing");
	windowOptions.Height = 800;
	windowOptions.Width = 800;
	windowOptions.AllowResize = false;

	GApi::UI::IGameWindow* pWindow = new GApi::OpenGL::Window::GLFWGameWindow();
	if (!pWindow->Initialize(windowOptions))
		return -1;

	pWindow->SetDebug(true);

	IGameEngine* pGameEngine = new GApi::Gaming::Engine::Mark1Engine();
	IGraphicsEngine* pGraphicsEngine = new OpenGLEngine();
	pGameEngine->SetGraphicsEngine(pGraphicsEngine);
	if (!pWindow->SetGameEngine(pGameEngine))
		return FALSE;
	
	if (!pWindow->Start())
		return -1;

	pWindow->Cleanup();
	SAFE_DELETE(pGraphicsEngine);
	SAFE_DELETE(pGameEngine);
	SAFE_DELETE(pWindow);
	
	return 0;
}

