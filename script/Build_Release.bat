mkdir ..\bin\ReleaseBuild

del /y ..\bin\ReleaseBuild\*.dll
del /y ..\bin\ReleaseBuild\*.exe

copy /y ..\bin\*.dll ..\bin\ReleaseBuild
copy /y ..\bin\*.exe ..\bin\ReleaseBuild

del ..\bin\ReleaseBuild\*_d.dll
del ..\bin\ReleaseBuild\*_d.exe

mkdir ..\bin\ReleaseBuild\shader
del /y ..\bin\ReleaseBuild\shader\*
copy /y ..\bin\shader\* ..\bin\ReleaseBuild\shader