mkdir ..\bin\DebugBuild

del /y ..\bin\ReleaseBuild\*.dll
del /y ..\bin\ReleaseBuild\*.exe

copy /y ..\bin\*_d.dll ..\bin\DebugBuild
copy /y ..\bin\*_d.exe ..\bin\DebugBuild

mkdir ..\bin\DebugBuild\shader
del /y ..\bin\ReleaseBuild\shader\*
copy /y ..\bin\shader\* ..\bin\DebugBuild\shader