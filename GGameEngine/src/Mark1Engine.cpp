#include "Mark1Engine.h"

#include "GFile.h"
#include "PathResolver.h"

using namespace GApi::Gaming::Engine;
using namespace GApi::System;

BOOL Mark1Engine::SetGraphicsEngine(IGraphicsEngine* pGraphicsEngine)
{
	m_pGraphicsEngine = pGraphicsEngine;
	return TRUE;
}

BOOL Mark1Engine::Initialize()
{
	return TRUE;
}

BOOL Mark1Engine::Start()
{
	GApi::Graphics::GraphicsEngineOptions options;
	options.width = 800;
	options.height = 800;
	if (!m_pGraphicsEngine->SetOptions(options))
		return FALSE;

	if (!m_pGraphicsEngine->Startup())
		return FALSE;

	char8 pszExecutableDirectory[256];
	if (!GApi::IO::GetExecutableDirectory(pszExecutableDirectory))
		return FALSE;

	GApi::IO::PathResolver pathResolver(pszExecutableDirectory);
	pathResolver.SetPath("SHADERS", "./shader/");


	GApi::Graphics::ResourceID fragmentShaderID;
	GApi::Graphics::ResourceID vertexShaderID;

	if (!m_pGraphicsEngine->RegisterShader(GApi::Graphics::ShaderStage::Vertex, pathResolver.GetFilePath("SHADERS", "vs_test.glsl"), vertexShaderID))
		return FALSE;

	if (!m_pGraphicsEngine->RegisterShader(GApi::Graphics::ShaderStage::Pixel, pathResolver.GetFilePath("SHADERS", "passColor.glsl"), fragmentShaderID))
		return FALSE;

	std::vector<GApi::Graphics::ResourceID> shaders = std::vector<GApi::Graphics::ResourceID>({ fragmentShaderID , vertexShaderID });
	if (!m_pGraphicsEngine->LinkShaders("PassedColor", shaders))
		return FALSE;

	if (!m_pGraphicsEngine->DeregisterShader(fragmentShaderID))
		return FALSE;

	if (!m_pGraphicsEngine->RegisterShader(GApi::Graphics::ShaderStage::Pixel, pathResolver.GetFilePath("SHADERS", "fs_test_other.glsl"), fragmentShaderID))
		return FALSE;

	shaders = std::vector<GApi::Graphics::ResourceID>({ fragmentShaderID , vertexShaderID });
	if (!m_pGraphicsEngine->LinkShaders("TestProgram", shaders))
		return FALSE;

	GApi::Graphics::Model<float32> model = GApi::Graphics::Square<float32>();
	GApi::Graphics::RGBColor blue(0.0f, 0.0f, 1.0f);
	GApi::Graphics::RGBColor blueColors[] = {
		blue,
		blue,
		blue,
		blue
	};
	model.SetColors(blueColors, 4);

	GApi::Graphics::ResourceID colloredResource;
	if (!m_pGraphicsEngine->RegisterModel(model, colloredResource))
		return FALSE;

	GApi::Graphics::ResourceID genericResource;

	if (!m_pGraphicsEngine->RegisterModel(model, genericResource))
		return FALSE;

	//if (!m_pGraphicsEngine->LinkShaderToModel("PassColorProgram", genericResource))
	//	return FALSE;

	if (!m_pGraphicsEngine->LinkShaderToModel("TestProgram", genericResource))
		return FALSE;
	if (!m_pGraphicsEngine->LinkShaderToModel("PassedColor", colloredResource))
		return FALSE;

	GApi::Graphics::Camera* pCamera = m_pGraphicsEngine->GetCameraPtr();
	pCamera->SetPosition(GApi::Graphics::Vector3D{ 0, 0, 10 });
	pCamera->SetLook(GApi::Graphics::Vector3D{ 0, 0, 0 });
	pCamera->SetUp(GApi::Graphics::Vector3D{ 0, 1, 0 });
	pCamera->SetFieldOfViewRadians(0.785398f);
	pCamera->SetAspectRatio(1.0f);
	pCamera->SetNearPlane(0.1f);
	pCamera->SetFarPlane(100.0f);

	return TRUE;
}

BOOL Mark1Engine::Stop()
{
	if (!m_pGraphicsEngine->Shutdown())
		return FALSE;

	return TRUE;
};

BOOL Mark1Engine::Cleanup()
{
	return TRUE;
}

BOOL Mark1Engine::UpdateState(const float32 deltaTime)
{
	static float32 paulFloat = 0.5f;

	// Game Logic
	for (uint32 i = 0; i < 1000; ++i)
	{
		float var = (float32)i / paulFloat;
		GApi::Graphics::Transformation transformation;
		transformation.SetScale(.25, .25, .25);
		transformation.SetTranslate(GApi::Graphics::Transformation::Axis::X, var);
		transformation.SetTranslate(GApi::Graphics::Transformation::Axis::Y, var);
		if (!m_pGraphicsEngine->ApplyTransformation(transformation, 2))
			return FALSE;
	}

	GApi::Graphics::Transformation transformation1;
	transformation1.SetScale(.25, .25, .25);
	transformation1.SetTranslate(GApi::Graphics::Transformation::Axis::X, paulFloat);
	if (!m_pGraphicsEngine->ApplyTransformation(transformation1, 2))
		return FALSE;

	GApi::Graphics::Transformation transformation2;
	transformation2.SetScale(.25, .25, .25);
	transformation2.SetTranslate(GApi::Graphics::Transformation::Axis::Y, paulFloat);
	transformation2.SetRotate(GApi::Graphics::Transformation::Axis::Z, paulFloat);
	if (!m_pGraphicsEngine->ApplyTransformation(transformation2, 2))
		return FALSE;

	GApi::Graphics::Transformation transformation3;
	transformation3.SetScale(.25, .25, .25);
	transformation3.SetTranslate(GApi::Graphics::Transformation::Axis::X, -paulFloat);
	if (!m_pGraphicsEngine->ApplyTransformation(transformation3, 1))
		return FALSE;

	GApi::Graphics::Transformation transformation4;
	transformation4.SetScale(.25, .25, .25);
	transformation4.SetTranslate(GApi::Graphics::Transformation::Axis::Y, -paulFloat);
	if (!m_pGraphicsEngine->ApplyTransformation(transformation4, 2))
		return FALSE;

	GApi::Graphics::Transformation transformation5;
	transformation5.SetScale(.25, .25, .25);
	transformation5.SetRotate(GApi::Graphics::Transformation::Axis::Z, -(paulFloat * 2));
	if (!m_pGraphicsEngine->ApplyTransformation(transformation5, 1))
		return FALSE;

	GApi::Graphics::Transformation transformation6;
	transformation6.SetScale(.25, .25, .25);
	transformation6.SetTranslate(GApi::Graphics::Transformation::Axis::X, paulFloat);
	transformation6.SetTranslate(GApi::Graphics::Transformation::Axis::Y, paulFloat);
	if (!m_pGraphicsEngine->ApplyTransformation(transformation6, 2))
		return FALSE;

	float distance = (1.0f*(deltaTime / 1000));
	paulFloat += distance;


	GApi::Graphics::Transformation shapeTransformation1;
	shapeTransformation1.SetScale(.25, .25, .25);
	shapeTransformation1.SetTranslate(GApi::Graphics::Transformation::Axis::X, 1.0f);
	if (!m_pGraphicsEngine->ApplyTransformation(shapeTransformation1, 1))
		return FALSE;

	GApi::Graphics::Transformation shapeTransformation2;
	shapeTransformation2.SetScale(.25, .25, .25);
	shapeTransformation2.SetTranslate(GApi::Graphics::Transformation::Axis::X, 1.5f);
	if (!m_pGraphicsEngine->ApplyTransformation(shapeTransformation2, 1))
		return FALSE;

	GApi::Graphics::Transformation shapeTransformation3;
	shapeTransformation3.SetScale(.25, .25, .25);
	shapeTransformation3.SetTranslate(GApi::Graphics::Transformation::Axis::X, 1.5f);
	shapeTransformation3.SetTranslate(GApi::Graphics::Transformation::Axis::Y, -0.5f);
	if (!m_pGraphicsEngine->ApplyTransformation(shapeTransformation3, 1))
		return FALSE;

	GApi::Graphics::Transformation shapeTransformation4;
	shapeTransformation4.SetScale(.25, .25, .25);
	shapeTransformation4.SetTranslate(GApi::Graphics::Transformation::Axis::X, 2.0f);
	shapeTransformation4.SetTranslate(GApi::Graphics::Transformation::Axis::Y, -0.5f);
	if (!m_pGraphicsEngine->ApplyTransformation(shapeTransformation4, 1))
		return FALSE;

	if (!m_pGraphicsEngine->Render())
		return FALSE;

	return TRUE;
}

BOOL Mark1Engine::HandleInputEvents(const PollInput* pInput)
{

	ComputerInput input = pInput->GetInputState();
	std::cout << "Mouse Pos" << input.m_CursorPosition[0] << ", " << input.m_CursorPosition[1] << std::endl;

	/*static float32 paulFloat = 0.5f;

	if (!inputEvents.empty())
	{
	GApi::Graphics::Transformation transformation1;
	transformation1.SetTranslate(GApi::Graphics::Transformation::Axis::X, paulFloat);
	if (!m_pGraphicsEngine->ApplyTransformation(transformation1, 1))
	return FALSE;

	GApi::Graphics::Transformation transformation2;
	transformation2.SetTranslate(GApi::Graphics::Transformation::Axis::Y, paulFloat);
	transformation2.SetRotate(GApi::Graphics::Transformation::Axis::Z, paulFloat);
	if (!m_pGraphicsEngine->ApplyTransformation(transformation2, 2))
	return FALSE;
	paulFloat += 0.01f;
	}*/


	return TRUE;
}

BOOL Mark1Engine::Pause()
{
	return TRUE;
}