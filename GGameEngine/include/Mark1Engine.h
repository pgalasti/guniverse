#ifndef G_MARK_1_GAME_ENGINE_H
#define G_MARK_1_GAME_ENGINE_H

#include "Defines.h"

#include <iostream>

#include "IGameEngine.h"
#include "GShapes.h"


using GApi::Gaming::IGameEngine;
using GApi::Gaming::InputEvent;
using GApi::Graphics::IGraphicsEngine;

namespace GApi {
	namespace Gaming {
		namespace Engine {

			class LibExport Mark1Engine : implements IGameEngine
			{
			public:
				virtual BOOL SetGraphicsEngine(IGraphicsEngine* pGraphicsEngine) override;
				
				virtual BOOL Initialize() override;
				virtual BOOL Start() override;
				virtual BOOL Stop() override;
				virtual BOOL Cleanup();

				virtual BOOL UpdateState(const float32 deltaTime) override;
				virtual BOOL HandleInputEvents(const PollInput* pInput) override;

				virtual BOOL Pause() override;

			private:
				IGraphicsEngine * m_pGraphicsEngine;
			};

		}
	}
}
#endif