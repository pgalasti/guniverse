#ifndef G_WINDOW_H
#define G_WINDOW_H

#include "Defines.h"

namespace GApi {
	namespace UI {

		struct LibExport WindowOptions
		{
			WindowOptions() { ZeroMemory(this, sizeof(WindowOptions)); }

			float32 Width;
			float32 Height;
			bool AllowResize;
			bool FullScreen;
			char8 WindowTitle[256];
		};

		interface LibExport IWindow
		{
			virtual ~IWindow() {}

			// Startup/Close
			virtual BOOL Initialize(const WindowOptions& options) = VIRTUAL;
			virtual BOOL Start() = VIRTUAL;
			virtual BOOL Cleanup() = VIRTUAL;

			// On window event
			virtual BOOL OnClose() = VIRTUAL;
			virtual BOOL OnResize() = VIRTUAL;

			// Misc
			virtual BOOL GetWindowHandle(void* ptr) = VIRTUAL;
			virtual void SetWindowTitle(const char8* pszName) = VIRTUAL;
		};
	}
}


#endif
