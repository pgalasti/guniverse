#ifndef G_COLOR_H
#define G_COLOR_H

#include "Defines.h"

namespace GApi {
	namespace Graphics {

		struct LibExport RGColor
		{
			RGColor(const float32 r, const float32 g) { this->r = r; this->g = g; }
			float32 r;
			float32 g;
		};

		struct LibExport RGBColor
		{
			RGBColor() { ZeroMemory(this, sizeof(RGBColor)); }
			RGBColor(const float32 r, const  float32 g, const float32 b) { this->r = r; this->g = g; this->b = b; }
			float32 r;
			float32 g;
			float32 b;
		};

		struct LibExport RGBAColor
		{
			RGBAColor(const float32 r, const float32 g, const float32 b, const float32 a) { this->r = r; this->g = g; this->b = b; this->a = a; }
			float32 r;
			float32 g;
			float32 b;
			float32 a;

			static RGBAColor DefaultBlue()
			{
				return RGBAColor(0.0f, 0.15f, 0.3f, 1.0f);
			}

			static RGBAColor DefaultGreen()
			{
				return RGBAColor(0.0f, 0.25f, 0.0f, 1.0f);
			}
		};

	}
}


#endif //G_COLOR_H

