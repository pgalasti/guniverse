#ifndef G_CAMERA_H
#define G_CAMERA_H

#include "Defines.h"

#include "GModel.h"

namespace GApi {
	namespace Graphics {

		using Vector3D = Vertex3D<float32>;

		class LibExport Camera
		{
		public:
			Camera();
			~Camera() {}

			inline void Initialize();

			inline const Vector3D GetPosition() const;
			inline const Vector3D GetLook() const;
			inline const Vector3D GetUp() const;

			void SetPosition(const Vector3D& position);
			void SetLook(const Vector3D& look);
			void SetUp(const Vector3D& up);

			inline const float32 GetAspectRatio() const;
			inline const float32 GetNearPlane() const;
			inline const float32 GetFarPlane() const;
			inline const float32 GetFieldOfViewRadians() const;

			inline void SetAspectRatio(const float32 aspectRatio);
			inline void SetNearPlane(const float32 nearPlane);
			inline void SetFarPlane(const float32 farPlane);
			inline void SetFieldOfViewRadians(const float32 fieldOfView);

		private:
			Vector3D m_Position;
			Vector3D m_Look;
			Vector3D m_Up;

			float32 m_ScreenAspectRatio;
			float32 m_NearPlane;
			float32 m_FarPlane;
			float32 m_FieldOfViewRadians;
		};

	}
}

#endif //G_CAMERA_H
