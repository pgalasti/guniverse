#ifndef G_ID_GENERATOR_H
#define G_ID_GENERATOR_H

#include "Defines.h"

#include <queue>
#include <set>

namespace GApi {
	namespace Util {

		template<typename T>
		class LibExport IDGenerator
		{

		private:
			std::set<T> m_IdSet;
			std::queue<T> m_IdQueue;
			T m_LastId;
			T m_FirstValue;
			T m_IncrementValue;


		public:

			IDGenerator(const T firstValue, const T incrementAmount = 100)
			{
				this->Reset(firstValue);
				this->m_IncrementValue = incrementAmount;

			}
			~IDGenerator()
			{
				m_IdSet.clear();
			}

			T GetNextId()
			{

				if (m_IdQueue.empty())
				{
					for (T i = m_LastId; i < m_LastId + m_IncrementValue; ++i)
						m_IdQueue.push(i);
				}

				// Get the next ID from the queue and pop it.
				m_LastId = m_IdQueue.front();
				m_IdQueue.pop();

				m_IdSet.insert(m_LastId);

				return m_LastId;
			}

			void Reset(const T firstValue)
			{
				//// Clear out any remaining values.
				//while (!m_IdQueue.empty())
				//	m_IdQueue.pop();

				//for (T i = firstValue; i < firstValue + m_IncrementValue; i++)
				//	m_IdQueue.push(i);

				m_IdSet.clear();
				m_FirstValue = m_LastId = firstValue;
			}


			void Release(const T id)
			{
				if (id > m_LastId)
					return; // Can't release something we never had assigned.

				//m_IdQueue.push_back(id);
				m_IdSet.erase(id);
			}

			T GetLatestId() { return m_LastId; }
			T GetFirstValue() { return m_FirstValue; }
			std::set<T> GetInUseIDs() { return m_IdSet;  }
		};
	}
}



#endif //G_ID_GENERATOR_H