#ifndef G_REDEFINES_H
#define G_REDEFINES_H

#include <string.h>
/** ZeroMemory **************************************************************/
#ifdef ZeroMemory
#undef ZeroMemory
#endif //ZeroMemory
#define ZeroMemory(p, s) memset(p, 0, s);

/****************************************************************************/


#endif // G_REDEFINES_H
