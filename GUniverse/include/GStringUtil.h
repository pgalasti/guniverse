#ifndef G_STRING_UTIL_H
#define G_STRING_UTIL_H

#include "Defines.h"

#include <string>
#include <list>

namespace GApi {
	namespace Util {

		LibExport const bool StringEndsWith(char8* const pszString, char8* const pszEndsWith, bool ignoreCase = false);

		LibExport const bool StringEndsWith(const std::string stringToCheck, const std::string endsWith, bool ignoreCase = false);
		
		//LibExport const std::wstring Convert(const std::string str);

		// Probably slow - but this won't be used for more than parsing small strings in non CPU critical conditions.
		class LibExport StringParser
		{
		public:
			StringParser();
			StringParser(std::string stringToParse, std::string strToken);
			~StringParser() {}

			void Parse(std::string stringToParse, std::string strToken);
			const std::string getToken() const;
			const uint32 getSize() const;
			const bool isFirst() const;
			const bool isLast() const;
			const bool getNext();
			const bool getBack();
			const bool getFirst();
			const bool getLast();
			const bool operator++(int);
			const bool operator--(int);

		protected:
			std::string m_OriginalString;
			std::list<std::string> m_TokenList;
			std::list<std::string>::iterator m_TokenListItr;
			std::string m_CurrentToken;

		private:
			void Init();
		};

	}
}

#endif //G_STRING_UTIL_H
