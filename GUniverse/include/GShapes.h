#ifndef G_SHAPES_H
#define G_SHAPES_H

#include "Defines.h"

#include "GModel.h"


namespace GApi {
	namespace Graphics {

		using GApi::Graphics::Vertex3D;

		template <typename T>
		LibExport Model<T> Square(const bool clockwise = false)
		{
			Model<T> model; 
			Vertex3D<T> pos1 = { -1,	 1,		0 };
			Vertex3D<T> pos2 = {  1,	 1,		0 };
			Vertex3D<T> pos3 = {  1,	-1,		0 };
			Vertex3D<T> pos4 = { -1,	-1,		0 };
			
			std::vector<Vertex3D<T>> positions = { pos1, pos2, pos3, pos4 };
			model.SetVerticies(positions);

			ushort16 indices[6] = {
				0, 1, 2,
				2, 3, 0
			};
			model.SetIndices(indices, 6);

			return model;
		}

	}
}

#endif //G_SHAPES_H