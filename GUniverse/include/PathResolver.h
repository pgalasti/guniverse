#ifndef G_PATH_RESOLVER_H
#define G_PATH_RESOLVER_H

#include "Defines.h"

#include <string>
#include <unordered_map>

namespace GApi {
	namespace IO {

		class LibExport PathResolver
		{
		public:
			PathResolver(const char8* pszPath);
			~PathResolver();

			void SetRootDirectory(const char8* pszPath);

			std::string GetPath(const char8* pszTag) const;
			void SetPath(const char8* pszTag, const char8* pszPath);
			std::string GetFilePath(const char8* pszTag, const char8* pszFileName) const;

			DISABLE_IMPLICIT_CONSTRUCTOR(PathResolver);
			DISABLE_COPY_CONSTRUCTOR(PathResolver);
			DISABLE_ASSIGNMENT_OPERATOR(PathResolver);

		private:
			std::unordered_map<std::string, std::string> m_TagMap;
			std::string m_RootDirectory;
		};

	}
}

#endif //G_PATH_RESOLVER_H