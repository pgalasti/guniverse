//#ifndef G_CLOCK_H
//#define G_CLOCK_H
//
//#include "Defines.h"
//
//#include <chrono>
//
//namespace GApi {
//	namespace Gaming {
//
//		class LibExport Clock
//		{
//		private: 
//			using HighResClock = std::chrono::high_resolution_clock;
//
//			template<typename Duration>
//			using HighResolutionTimePoint = std::chrono::time_point<HighResClock, Duration>;
//
//			static HighResolutionTimePoint<std::chrono::nanoseconds> m_lastTimePoint;
//		public:
//			Clock();
//			
//			enum TimeUnitsEnum
//			{
//				NanoSeconds,
//				Microseconds,
//				MilliSeconds,
//				Seconds,
//				Minutes
//			};
//
//			static const long64 GetTime(const TimeUnitsEnum timeUnit);
//
//			const long64 Tick(const TimeUnitsEnum timeUnit) const;
//			
//		};
//
//	}
//}
//
//#endif //G_CLOCK_H