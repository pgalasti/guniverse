#ifndef G_FILE_H
#define G_FILE_H

#include "Defines.h"

#include <stdio.h>

#ifdef _WIN32
#define FILE_SEPERATOR_CHAR '\\'
#elif LINUX
#define FILE_SEPERATOR_CHAR '/'
#endif

namespace GApi {
	namespace IO {

		LibExport BOOL LoadFile(const char8* pszFilePath, char8* pszFileOutput, const uint32 maxLength);

		LibExport BOOL GetExecutableDirectory(char8* pszDirectoryOutput);
	}
}



#endif //G_FILE_H
