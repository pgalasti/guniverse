#ifndef G_MODEL_H
#define G_MODEL_H

#include "Defines.h"

#include "GColor.h"

#include <vector>

namespace GApi {
	namespace Graphics {

		template <typename Type, const ushort16 Length>
		struct LibExport Section
		{
			Type data[Length];
		};

		template <typename Type>
		using Vertex2D = Section<Type, 2>;
		template <typename Type>
		using Vertex3D = Section<Type, 3>;
		template <typename Type>
		using Vertex4 = Section<Type, 4>;

		template <typename Type>
		struct LibExport Model
		{
			std::vector<Vertex3D<Type>> Positions;
			std::vector<ushort16> Indices;
			std::vector<RGBColor> Colors;

			Model& operator=(const Model& other)
			{
				Positions = other.Positions;
				Indices = other.Indices;
				Colors = other.Colors;

				return *this;
			}

			void SetVerticies(const Vertex3D<Type>* pVerticies, const uint32 numVerticies)
			{
				Positions.reserve(numVerticies);

				Vertex3D<Type> vertex;
				for (uint32 i = 0; i < numVerticies; ++i)
				{
					memcpy(&vertex, pVerticies + i, sizeof(Vertex3D<Type>));
					Positions.push_back(vertex);
				}
			}

			void SetVerticies(const std::vector<Vertex3D<Type>>& verticies)
			{
				Positions.reserve(verticies.size());
				Positions = verticies;
			}

			void SetIndices(const ushort16* pIndices, const uint32 numIndices)
			{
				Indices.reserve(numIndices);

				for (uint32 i = 0; i < numIndices; ++i)
				{
					Indices.push_back(pIndices[i]);
				}
			}

			void SetIndices(const std::vector<ushort16>& indices)
			{
				Indices = indices;
			}

			void SetColors(const RGBColor* pColors, const uint32 numColors)
			{
				Colors.reserve(numColors);

				RGBColor color;
				for (uint32 i = 0; i < numColors; ++i)
				{
					memcpy(&color, pColors + i, sizeof(RGBColor));
					Colors.push_back(color);
				}
			}

			void SetColors(const std::vector<RGBColor>& colors)
			{
				Colors.reserve(colors.size());
				Colors = colors;
			}

		};

	}
}

#endif // G_MODEL_H
