#ifndef G_GAME_WINDOW_H
#define G_GAME_WINDOW_H

#include "Defines.h"

#include <string>
#include <memory>

#include "GWindow.h"
#include "IGameEngine.h"

namespace GApi {
	namespace UI {

		using GApi::Gaming::IGameEngine;

		// Rename to GameWindowBase
		interface LibExport IGameWindow : implements IWindow
		{
		public:
			virtual ~IGameWindow() {}

			// Startup/Close
			virtual BOOL Initialize(const WindowOptions& options) override = VIRTUAL;
			virtual BOOL Start() override = VIRTUAL;
			virtual BOOL Cleanup() override = VIRTUAL;

			// On window event
			virtual BOOL OnClose() = VIRTUAL;
			virtual BOOL OnResize() = VIRTUAL;
			
			// Misc
			virtual void SetWindowTitle(const char8* pszName) = VIRTUAL;
			virtual BOOL GetWindowHandle(void* ptr) = VIRTUAL;
			virtual void SetDebug(const bool enable) = VIRTUAL;

			// Game state
			virtual inline bool IsRunning() const = VIRTUAL;
			virtual float32 FPS() const = VIRTUAL; 
			virtual BOOL HandleEvents() = VIRTUAL;
			virtual BOOL UpdateState() = VIRTUAL;
			virtual BOOL Run(const float32 deltaTime) = VIRTUAL;

			virtual BOOL SetGameEngine(IGameEngine* pEngine) = VIRTUAL;
		};
	}
}
#endif // G_GAME_WINDOW_H
