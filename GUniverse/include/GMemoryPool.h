#ifndef G_MEMORY_POOL_H
#define G_MEMORY_POOL_H

#include "Defines.h"
#include <list>

namespace GApi {
	namespace Memory {

		// Grabbed from Game Coding Complete http://www.mcshaffry.com/GameCode/
		class LibExport MemoryPool
		{
			uChar8** m_ppRawMemoryArray;  // an array of memory blocks, each split up into chunks and connected
			uChar8* m_pHead;  // the front of the memory chunk linked list
			uint32 m_chunkSize, m_numChunks;  // the size of each chunk and number of chunks per array, respectively
			uint32 m_memArraySize;  // the number elements in the memory array
			bool m_toAllowResize;  // true if we resize the memory pool when it fills up

								   // tracking variables we only care about for debug
		public:
			// construction
			MemoryPool(void);
			~MemoryPool(void);
			bool Init(uint32 chunkSize, uint32 numChunks);
			void Destroy(void);

			// allocation functions
			void* Alloc(void);
			void Free(void* pMem);
			uint32 GetChunkSize(void) const { return m_chunkSize; }

			// settings
			void SetAllowResize(bool toAllowResize) { m_toAllowResize = toAllowResize; }


		private:
			// resets internal vars
			void Reset(void);

			// internal memory allocation helpers
			bool GrowMemoryArray(void);
			uChar8* AllocateNewMemoryBlock(void);

			// internal linked list management
			uChar8* GetNext(uChar8* pBlock);
			void SetNext(uChar8* pBlockToChange, uChar8* pNewNext);

			// don't allow copy constructor
			MemoryPool(const MemoryPool& memPool) {}
		};
	}
}

#endif //G_MEMORY_POOL_H