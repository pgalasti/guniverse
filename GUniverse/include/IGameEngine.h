#ifndef G_GAME_ENGINE_H
#define G_GAME_ENGINE_H

#include "Defines.h"

#include "GGraphicsEngine.h"
#include "GInput.h"
#include <vector>

namespace GApi {
	namespace Gaming {

		using GApi::Graphics::IGraphicsEngine;
		using GApi::System::PollInput;

		struct LibExport InputEvent 
		{
			double64 MousePositionX;
			double64 MousePositionY;
			int32 keyUp;
			int32 keyDown;
		};

		interface LibExport IGameEngine
		{
			virtual ~IGameEngine() {}

			/// A pointer to the derived graphics engine used in implementation.
			virtual BOOL SetGraphicsEngine(IGraphicsEngine* pGraphicsEngine) = VIRTUAL;

			/** Initialization/Closing **/
			/// Implementation for initialization of resources before game engine starts.
			virtual BOOL Initialize() = VIRTUAL;
			/// Implementation for starting of the game engine.
			virtual BOOL Start() = VIRTUAL;
			/// Implementation for stoping of the game engine.
			virtual BOOL Stop() = VIRTUAL;
			/// Implementation for cleaning up resources used by game engine.
			virtual BOOL Cleanup() = VIRTUAL;

			/** State updating and event handling **/
			/// Implementation for state updates of the game based on delta from last state update
			virtual BOOL UpdateState(const float32 deltaTime) = VIRTUAL;
			/// Implementation for handling input events of structure InputEvent
			virtual BOOL HandleInputEvents(const PollInput* pInput) = VIRTUAL;
			/// Implementation for pausing the game engine
			virtual BOOL Pause() = VIRTUAL;
		};

	}
}

#endif //G_GAME_ENGINE_H