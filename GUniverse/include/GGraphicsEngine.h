#ifndef G_GRAPHICS_ENGINE_H
#define G_GRAPHICS_ENGINE_H

#include "Defines.h"

#include <string>

#include "GColor.h"
#include "GModel.h"
#include "GTransformation.h"
#include "GCamera.h"

namespace GApi {
	namespace Graphics {

		struct LibExport GraphicsEngineOptions
		{
			bool FullScreen;
			float32 width;
			float32 height;
			ubyte8 swapChainCount;
		};

		enum LibExport PolygonRenderEnum
		{
			Point,
			Line,
			Fill
		};

		struct LibExport RenderOptions
		{
			PolygonRenderEnum PolygonRendering;
		};

		struct LibExport EngineStateInfo
		{
			uint32 Verticies;
			uint32 Indices;
			uint32 Transformations;
		};

		enum LibExport ShaderStage
		{
			Vertex,
			Tesselation,
			Geometry,
			Pixel
		};

		typedef uint32 ResourceID;

		interface LibExport IGraphicsEngine
		{
			virtual ~IGraphicsEngine() {}

			// Window Management
			virtual BOOL OnResize(const float32 width, const float32 height) = VIRTUAL;

			// Options
			virtual BOOL SetOptions(const GraphicsEngineOptions& options) = VIRTUAL;
			virtual BOOL SetWindowHandle(void* ptr) = VIRTUAL;

			// Initialize/Shutdown
			virtual BOOL Startup() = VIRTUAL;
			virtual BOOL Shutdown() = VIRTUAL;

			// Resource Registration
			virtual BOOL RegisterModel(const Model<float32>& model, ResourceID& resourceID) = VIRTUAL;
			virtual BOOL DeregisterModel(const ResourceID resourceID) = VIRTUAL;

			virtual BOOL RegisterShader(const ShaderStage shader, const std::string& path, ResourceID& resourceID) = VIRTUAL;
			virtual BOOL DeregisterShader(const ResourceID resourceID) = VIRTUAL;

			virtual BOOL LinkShaders(const std::string& alias, std::vector<ResourceID>& resources) = VIRTUAL;
			virtual BOOL DeleteLinkedShaders(const std::string& alias) = VIRTUAL;
			virtual BOOL LinkShaderToModel(const std::string& alias, const ResourceID resourceID) = VIRTUAL;

			// State Modification
			virtual BOOL ApplyTransformation(const Transformation& transformation, const ResourceID resourceID) = VIRTUAL;

			// Rendering
			virtual BOOL Render() = VIRTUAL;
			virtual BOOL ClearScreen(const RGBAColor& clearColor) = VIRTUAL;
			virtual BOOL SetRendering(const RenderOptions& options) = VIRTUAL;

			// Misc
			virtual std::string GetEngineDescription() const = VIRTUAL;
			virtual Camera* GetCameraPtr() = VIRTUAL;
			virtual EngineStateInfo GetEngineState() const = VIRTUAL;
		};

	}
}



#endif //G_GRAPHICS_ENGINE_H
