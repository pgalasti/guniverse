#ifndef G_WIN32_CONSOLE_WINDOW_H
#define G_WIN32_CONSOLE_WINDOW_H

#include "Defines.h"

#include "GWindow.h"

using GApi::UI::IWindow;

namespace GApi {
	namespace UI {

		class LibExport Win32ConsoleWindow : implements IWindow
		{
		public:

			// Startup/Close
			virtual BOOL Initialize(const WindowOptions& options) override;
			virtual BOOL Start() override;
			virtual BOOL Cleanup() override;

			// On window event
			virtual BOOL OnClose() override;
			virtual BOOL OnResize() override;

			// Misc
			virtual BOOL GetWindowHandle(void* ptr) override;
			virtual void SetWindowTitle(const char8* pszName) override;
		};

	}
}

#endif //G_WIN32_CONSOLE_WINDOW_H
