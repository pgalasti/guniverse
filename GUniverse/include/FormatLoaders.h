#ifndef G_FORMAT_LOADER_H
#define G_FORMAT_LOADER_H

#include "Defines.h"

#include "GModel.h"

namespace GApi {
	namespace Graphics {

		interface LibExport IFormatLoader 
		{
			virtual BOOL LoadFile(const char* pszPath, Model<float32>& model) = VIRTUAL;
		};

		class LibExport ObjFormatLoader : public IFormatLoader
		{
		public:

			virtual BOOL LoadFile(const char* pszPath, Model<float32>& model) override;

		private:
			Model<float32> ParseText(const char8* pszFileOutput);
			void ParseLine(const std::string& line, Model<float32>& model);

			static const char8* COMMENT;
			static const char8* VERTEX;
			static const char8* NORMAL;
			static const char8* INDEX;
			static const char8* TEXTCOORD;
			static const char8* OBJECT;
		};

	}
}

#endif //G_FORMAT_LOADER_H