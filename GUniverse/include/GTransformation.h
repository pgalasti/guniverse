#ifndef G_TRANSFORMATION_H
#define G_TRANSFORMATION_H

#include "Defines.h"

namespace GApi {
	namespace Graphics {

		struct LibExport Transformation
		{
			enum Axis {
				X,
				Y,
				Z
			};

			Transformation();

			bool HasScale;
			bool HasTranslate;
			bool HasRotate;
			bool HasRotateX;
			bool HasRotateY;
			bool HasRotateZ;


			float32 Scale[3];
			float32 Translate[3];
			float32 Rotate[3];

			void SetScale(Axis axis, const float32 scale);
			void SetScale(const float32 x, const float32 y, const float32 z);

			void SetTranslate(Axis axis, const float32 translate);
			void SetTranslate(const float32 x, const float32 y, const float32 z);

			void SetRotate(Axis axis, const float32 rotate);
		};

	}
}

#endif //G_TRANSFORMATION_H