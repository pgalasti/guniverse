#include "GStringUtil.h"

#include <codecvt>

using namespace GApi::Util;

const bool GApi::Util::StringEndsWith(char8* const pszString, char8* const pszEndsWith, bool ignoreCase/* = false*/)
{
	const uint32 nSize = (uint32)strlen(pszString);
	const uint32 checkStringSize = (uint32)strlen(pszEndsWith);
	if (nSize < checkStringSize)
		return false;

	char8* pString = pszString;
	char8* pCheckString = pszEndsWith;

	pString += nSize - checkStringSize;

	for (uint32 i = 0; i < checkStringSize; ++i)
	{
		if (ignoreCase)
		{
			if (tolower(pString[i]) != tolower(pCheckString[i]))
				return false;
		}
		else
		{
			if (pString[i] != pCheckString[i])
				return false;
		}
	}

	return true;
}

const bool GApi::Util::StringEndsWith(const std::string stringToCheck, const std::string endsWith, bool ignoreCase /*= false*/)
{
	char8* const pszString = (char8*)stringToCheck.c_str();
	char8* const pszEndsWith = (char8*)endsWith.c_str();

	return StringEndsWith(pszString, pszEndsWith, ignoreCase);
}

//const std::wstring GApi::Util::Convert(const std::string str)
//{
//	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
//	return converter.from_bytes(str);
//}

StringParser::StringParser()
{
	Init();
}

StringParser::StringParser(std::string stringToParse, std::string strToken)
{
	Init();
	Parse(stringToParse, strToken);
}

void StringParser::Init()
{
	m_OriginalString.clear();
	m_CurrentToken.clear();
	m_TokenList.clear();
	m_TokenListItr = m_TokenList.begin();
}

void StringParser::Parse(std::string stringToParse, std::string strToken)
{
	Init();
	if (stringToParse.empty())
		return;

	size_t startPosition = 0, currentPosition = 0;

	while ((currentPosition = stringToParse.find_first_of(strToken, currentPosition)) != std::string::npos)
	{
		m_CurrentToken = stringToParse.substr(startPosition, currentPosition - startPosition);
		m_TokenList.push_back(m_CurrentToken);
		startPosition = ++currentPosition;
	}

	m_CurrentToken = stringToParse.substr(startPosition, currentPosition - startPosition);
	m_TokenList.push_back(m_CurrentToken);
}

const std::string StringParser::getToken() const
{
	return m_CurrentToken;
}

const uint32 StringParser::getSize() const
{
	return (uint32)m_TokenList.size();
}

const bool StringParser::isFirst() const
{
	return m_TokenListItr == m_TokenList.begin();
}

const bool StringParser::isLast() const
{
	auto endIter = m_TokenList.end();
	return m_TokenListItr == --endIter;
}

const bool StringParser::getNext()
{
	if (++m_TokenListItr == m_TokenList.end())
	{
		m_CurrentToken.clear();
		return false;
	}

	m_CurrentToken = *m_TokenListItr;
	return true;
}

const bool StringParser::getBack()
{
	if (this->isFirst())
	{
		m_CurrentToken.clear();
		return false;
	}

	--m_TokenListItr;
	m_CurrentToken = *m_TokenListItr;
	return true;
}

const bool StringParser::getFirst()
{
	if (m_TokenList.empty())
	{
		m_CurrentToken.clear();
		return false;
	}

	m_TokenListItr = m_TokenList.begin();
	m_CurrentToken = *m_TokenListItr;
	return true;
}

const bool StringParser::getLast()
{
	if (m_TokenList.empty())
	{
		m_CurrentToken.clear();
		return false;
	}

	m_TokenListItr = m_TokenList.end();
	--m_TokenListItr;
	m_CurrentToken = *m_TokenListItr;
	return true;
}

const bool StringParser::operator++(int)
{
	return getNext();
}

const bool StringParser::operator--(int)
{
	return getBack();
}