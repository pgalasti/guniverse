#include "GFile.h"

#ifdef _WIN32
#include <Windows.h>
#include "Shlwapi.h"
#endif //_WIN32

BOOL GApi::IO::LoadFile(const char8* pszFilePath, char8* pszFileOutput, const uint32 maxLength)
{
	FILE* pFile = fopen(pszFilePath, "r+");
	if (IsNull(pFile))
		return FALSE;

	ZeroMemory(pszFileOutput, maxLength);
	char8 szLine[1024];
	while (fgets(szLine, 1024, pFile))
		strcat(pszFileOutput, szLine);

	C_FILE_CLOSE(pFile);

	return TRUE;
}


BOOL GApi::IO::GetExecutableDirectory(char8* pszDirectoryOutput)
{
#ifdef _WIN32
	char szPath[_MAX_PATH];
	GetModuleFileName(NULL, szPath, _MAX_PATH);

	char szDrive[_MAX_PATH];
	char szDirectory[_MAX_PATH];
	_splitpath(szPath, szDrive, szDirectory, nullptr, nullptr);
	strcpy(pszDirectoryOutput, szDrive);
	strcat(pszDirectoryOutput, szDirectory);
#elif LINUX
	char szPath[1024];
	readlink("/proc/self/exe", szPath, 1024);
	directory = dirname(szPath);
#endif
	
	return TRUE;
}