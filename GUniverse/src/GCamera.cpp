#include "GCamera.h"

using namespace GApi::Graphics;

Camera::Camera()
{
	this->Initialize();
}

void Camera::Initialize()
{
	ZeroMemory(this, sizeof(Camera));
}

const Vector3D Camera::GetPosition() const
{
	return m_Position;
}

const Vector3D Camera::GetLook() const
{
	return m_Look;
}

const Vector3D Camera::GetUp() const
{
	return m_Up;
}

void Camera::SetPosition(const Vector3D& position)
{
	m_Position = position;
}

void Camera::SetLook(const Vector3D& look)
{
	m_Look = look;
}

void Camera::SetUp(const Vector3D& up)
{
	m_Up = up;
}

const float32 Camera::GetAspectRatio() const
{
	return m_ScreenAspectRatio;
}

const float32 Camera::GetNearPlane() const
{
	return m_NearPlane;
}

const float32 Camera::GetFarPlane() const
{
	return m_FarPlane;
}

const float32 Camera::GetFieldOfViewRadians() const
{
	return m_FieldOfViewRadians;
}

void Camera::SetAspectRatio(const float32 aspectRatio)
{
	m_ScreenAspectRatio = aspectRatio;
}

void Camera::SetNearPlane(const float32 nearPlane)
{
	m_NearPlane = nearPlane;
}

void Camera::SetFarPlane(const float32 farPlane)
{
	m_FarPlane = farPlane;
}

void Camera::SetFieldOfViewRadians(const float32 fieldOfView)
{
	m_FieldOfViewRadians = fieldOfView;
}