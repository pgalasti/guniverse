#include "PathResolver.h"
#include "GFile.h"

using namespace GApi::IO;

PathResolver::PathResolver(const char8* pszPath)
{
	this->SetRootDirectory(pszPath);
}

PathResolver::~PathResolver()
{

}

void PathResolver::SetRootDirectory(const char8* pszPath)
{
	m_RootDirectory = pszPath;
}

std::string PathResolver::GetPath(const char8* pszTag) const
{
	auto iter = m_TagMap.find(std::string(pszTag));
	if (iter == m_TagMap.end())
		return "";

	return iter->second;
}

void PathResolver::SetPath(const char8* pszTag, const char8* pszPath)
{
	m_TagMap[std::string(pszTag)] = std::string(pszPath);
}

std::string PathResolver::GetFilePath(const char8* pszTag, const char8* pszFileName) const
{
	std::string filePath = m_RootDirectory + GetPath(pszTag);
	//filePath += FILE_SEPERATOR_CHAR;
	filePath += pszFileName;
	
	return filePath;
}