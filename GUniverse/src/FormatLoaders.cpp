#include "FormatLoaders.h"

#include "GFile.h"
#include "GStringUtil.h"

using namespace GApi::Graphics;

const char* ObjFormatLoader::COMMENT	= "#";
const char* ObjFormatLoader::VERTEX		= "v";
const char* ObjFormatLoader::NORMAL		= "vn";
const char* ObjFormatLoader::INDEX		= "f";
const char* ObjFormatLoader::TEXTCOORD	= "vt";
const char* ObjFormatLoader::OBJECT		= "o";

BOOL ObjFormatLoader::LoadFile(const char* pszPath, Model<float32>& model)
{
	char8 pszFileOutput[10240];
	if (!GApi::IO::LoadFile(pszPath, pszFileOutput, 10240))
		return FALSE;

	model = ParseText(pszFileOutput);

	return TRUE;
}

Model<float32> ObjFormatLoader::ParseText(const char8* pszFileOutput)
{
	Model<float32> model;

	GApi::Util::StringParser fileParser(std::string(pszFileOutput), std::string("\n"));

	if (!fileParser.getFirst())
		return model;

	
	do 
	{
		const std::string line = fileParser.getToken();
		ParseLine(line, model);
	} while (fileParser.getNext());

	return model;
}

void ObjFormatLoader::ParseLine(const std::string& line, Model<float32>& model)
{
	GApi::Util::StringParser lineParser(line, " ");
	if (!lineParser.getFirst())
		return;

	const std::string lineType = lineParser.getToken();
	if (lineType == ObjFormatLoader::COMMENT)
	{
		return; // Just ignore
	}
	else if (lineType == ObjFormatLoader::VERTEX)
	{
		GApi::Graphics::Vertex3D<float32> vertex;
		ushort16 position = 0;
		while (lineParser++)
		{
			if (position > 3)
				continue;

			std::string strPosition = lineParser.getToken();
			vertex.data[position++] = (float32)atof(strPosition.c_str());
		}
		model.Positions.push_back(vertex);
	}
	else if (lineType == ObjFormatLoader::NORMAL)
	{

	}
	else if (lineType == ObjFormatLoader::INDEX)
	{

	}
	else if (lineType == ObjFormatLoader::TEXTCOORD)
	{

	}
	else if (lineType == ObjFormatLoader::OBJECT)
	{

	}
}