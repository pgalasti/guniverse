#include "GTransformation.h"

using namespace GApi::Graphics;

Transformation::Transformation()
{
	ZeroMemory(this, sizeof(Transformation));
}

void Transformation::SetScale(Axis axis, const float32 scale)
{
	HasScale = true;
	switch (axis)
	{
	case X:
		this->Scale[0] = scale;
		return;
	case Y:
		this->Scale[1] = scale;
		return;
	case Z:
		this->Scale[2] = scale;
		return;
	}
}

void Transformation::SetScale(const float32 x, const float32 y, const float32 z)
{
	HasScale = true;
	this->Scale[0] = x;
	this->Scale[1] = y;
	this->Scale[2] = z;
}

void Transformation::SetTranslate(Axis axis, const float32 translate)
{
	HasTranslate = true;
	switch (axis)
	{
	case X:
		this->Translate[0] = translate;
		return;
	case Y:
		this->Translate[1] = translate;
		return;
	case Z:
		this->Translate[2] = translate;
		return;
	}
}

void Transformation::SetTranslate(const float32 x, const float32 y, const float32 z)
{
	HasTranslate = true;
	this->Translate[0] = x;
	this->Translate[1] = y;
	this->Translate[2] = z;
}

void Transformation::SetRotate(Axis axis, const float32 rotate)
{
	HasRotate = true;
	switch (axis)
	{
	case X:
		HasRotateX = true;
		this->Rotate[0] = rotate;
		return;
	case Y:
		HasRotateY = true;
		this->Rotate[1] = rotate;
		return;
	case Z:
		HasRotateZ = true;
		this->Rotate[2] = rotate;
		return;
	}
}