//#include "GClock.h"
//
//#include <chrono>
//
//using GApi::Gaming::Clock;
//
//
//
//
//Clock::Clock()
//{
//	m_lastTimePoint = HighResClock::now();
//}
//
//const long64 Clock::GetTime(const TimeUnitsEnum timeUnit)
//{
//	auto timePoint = HighResClock::now();
//
//	switch (timeUnit)
//	{
//	case NanoSeconds:
//		return HighResolutionTimePoint<std::chrono::nanoseconds>(timePoint).time_since_epoch().count();
//	case Microseconds:
//		return std::chrono::duration_cast<std::chrono::microseconds>(timePoint.time_since_epoch()).count();
//	case MilliSeconds:
//		return std::chrono::duration_cast<std::chrono::milliseconds>(timePoint.time_since_epoch()).count();
//	case Seconds:
//		return std::chrono::duration_cast<std::chrono::seconds>(timePoint.time_since_epoch()).count();
//	case Minutes:
//		return std::chrono::duration_cast<std::chrono::minutes>(timePoint.time_since_epoch()).count();
//	}
//	
//	return -1;
//}
//
//const long64 Clock::Tick(const TimeUnitsEnum timeUnit) const
//{
//	auto newTimePoint = HighResClock::now();
//	auto result = newTimePoint - m_lastTimePoint;
//	
//	m_lastTimePoint = HighResClock::now();
//
//	switch (timeUnit)
//	{
//	case NanoSeconds:
//		return std::chrono::duration_cast<std::chrono::nanoseconds>(result).count();
//	case Microseconds:
//		return std::chrono::duration_cast<std::chrono::microseconds>(result).count();
//	case MilliSeconds:
//		return std::chrono::duration_cast<std::chrono::milliseconds>(result).count();
//	case Seconds:
//		return std::chrono::duration_cast<std::chrono::seconds>(result).count();
//	case Minutes:
//		return std::chrono::duration_cast<std::chrono::minutes>(result).count();
//	}
//	
//	return -1;
//}