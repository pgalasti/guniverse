#ifdef _WIN32

#include "Win32ConsoleWindow.h"

#include "Windows.h"

using GApi::UI::Win32ConsoleWindow;
using GApi::UI::WindowOptions;

BOOL Win32ConsoleWindow::Initialize(const WindowOptions& options)
{

	return TRUE;
}

BOOL Win32ConsoleWindow::Start()
{
	return AllocConsole();
	return TRUE;
}

BOOL Win32ConsoleWindow::Cleanup()
{

	return TRUE;
}

BOOL Win32ConsoleWindow::OnClose()
{

	return TRUE;
}

BOOL Win32ConsoleWindow::OnResize()
{

	return TRUE;
}

BOOL Win32ConsoleWindow::GetWindowHandle(void* ptr)
{

	return TRUE;
}

void Win32ConsoleWindow::SetWindowTitle(const char8* pszName)
{

}

#endif //_WIN32